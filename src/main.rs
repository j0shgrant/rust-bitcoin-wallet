use ring::rand::{SystemRandom, SecureRandom};
use ripemd160::{Digest, Ripemd160};
use secp256k1::{PublicKey, Secp256k1, SecretKey};
use sha2::Sha256;

fn apply_sha(key: &Vec<u8>) -> [u8; 32] {
    let mut output = [0; 32];
    let mut hasher = Sha256::new();
    hasher.update(&key[..]);
    output.copy_from_slice(&hasher.finalize()[..]);
    return output;
}

fn apply_ripemd(key: [u8; 32]) -> [u8; 20] {
    let mut output = [0; 20];
    let mut hasher = Ripemd160::new();
    hasher.update(key);
    output.copy_from_slice(&hasher.finalize()[..]);
    return output;
}

fn main() {
    let secp = Secp256k1::new();
    let s_rand: SystemRandom = SystemRandom::new();

    let mut private_key_buff = [0; 32];
    match s_rand.fill(&mut private_key_buff) {
        Ok(_) => println!("Successfully generated random key"),
        Err(err) => panic!(err)
    }
    let private_key: SecretKey = match SecretKey::from_slice(&private_key_buff) {
        Ok(key) => key,
        Err(err) => panic!(err)
    };
    println!("Generated private key: {}", hex::encode(private_key_buff));

    let public_key: PublicKey = PublicKey::from_secret_key(&secp, &private_key);
    println!("Generated corresponding public key: {}", hex::encode(&public_key.serialize()[..]));

    // Apply hashing algorithms to the generated public key
    let mut public_key_hex: Vec<u8> = vec![0; 33];
    public_key_hex.copy_from_slice(&public_key.serialize());
    let hashed_key = apply_ripemd(apply_sha(&public_key_hex));
    println!("Hashed corresponding public key: {}", hex::encode(hashed_key));
}
